from django.http.response import HttpResponse, JsonResponse
from django.core import serializers
from django.shortcuts import get_object_or_404, render
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.
def index(request):
    notes = Note.objects.all()  
    response = {'notes': notes}
    return render(request, 'lab5_index.html', response)


def get_note(request, id):
    data = serializers.serialize('json', [Note.objects.get(id = id),])
    return HttpResponse(data, content_type="application/json")


def update_note(request, id):
    obj = get_object_or_404(Note, id = id)

    if request.is_ajax and request.method == "POST":
        form = NoteForm(request.POST, instance=obj)

        if form.is_valid():
            data = serializers.serialize('json', [form.save(),])
            return JsonResponse({"instance": data}, status=200)
        else:
            return JsonResponse({"error": form.errors}, status=400)
    
    return JsonResponse({"error": "Edit Failed"}, status=400)


def delete_note(request, id):
    obj = get_object_or_404(Note, id = id)

    if request.method == "POST":
        obj.delete()
    
    data = serializers.serialize('json', Note.objects.all())
    return HttpResponse(data, content_type='application/json')