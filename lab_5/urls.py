from django.urls import path
from .views import delete_note, get_note, index, update_note

urlpatterns = [
    path('', index, name='index_lab5'),
    path('notes/<id>', get_note),
    path('notes/<id>/update', update_note),
    path('notes/<id>/delete', delete_note)
]