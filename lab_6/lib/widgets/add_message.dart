import 'package:flutter/material.dart';

class AddMessage extends StatefulWidget {
  const AddMessage({Key? key}) : super(key: key);

  @override
  _AddMessageState createState() => _AddMessageState();
}

class _AddMessageState extends State<AddMessage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child:
        Column(
          children: <Widget>[
            const SizedBox(height: 18,),
            const Text("Title", style:
            TextStyle(
              fontWeight: FontWeight.normal,
              fontSize: 13,
            ),
            textAlign: TextAlign.center
            ),
            // const SizedBox(height: 18),
            Padding(padding: EdgeInsets.all(20.0),
              child: TextFormField(
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
                  hintText: "Title",
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5),
                  )
                ),
              ),
            ),

            const SizedBox(height: 18,),
            const Text("Message:", style:
              TextStyle(
                fontWeight: FontWeight.normal,
                fontSize: 13,
                ),
              textAlign: TextAlign.left,
            ),
            // const SizedBox(height: 18),
            Padding(padding: EdgeInsets.all(20.0),
              child: TextFormField(
                decoration: InputDecoration(
                    hintText: "Message",
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5),
                    )
                ),
              ),
            ),

            const SizedBox(height: 12,),
            ElevatedButton(
              style: ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 13),
                primary: Colors.blueGrey,
                onPrimary: Colors.white,
                padding: EdgeInsets.only(left: 12, right: 12, top: 8, bottom: 8),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),),
              onPressed: () {},
              child: const Text('Tambahkan Pesan'),
            ),
            const SizedBox(height: 18,),
          ],
        )
    );
  }
}
