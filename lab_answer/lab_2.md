1. Apakah perbedaan antara JSON dan XML?
JSON dan XML mempunyai banyak perbedaan, seperti berikut:
- Bahasa
    JSON adalah format yang ditulis dalam JavaScript. Sedangkan XML adalah bahasa markup yang memiliki tag untuk mendefinisikan elemen.
- Pengolahan 
    Dalam pengolahannya JSON tidak melakukan pemrosesan atau perhitungan. Sedangkan XML dapat melakukan pemformatan dokumen dan objek.
- Penyimpanan Data
    Pada JSON data disimpan seperti map yang mempunyai key dan value. Sedangkan pada XML, data disimpan seperti tree structure.


2. Apakah perbedaan antara HTML dan XML?
HTML dan XML mempunyai banyak perbedaan, seperti berikut:
- Tujuan 
    HTML bertujuan untuk menyajikan data. Sedangkan XML bertujuan untuk mentransfer informasi.
- Tag Penutup
    Pada HTML tag penutup bersifat opsional, dapat diberikan atau tidak. Sedangkan pada XML wajib menggunakan tag penutup.
- Urutan/nesting
    Pada HTML urutan tidak diperlukan. Sedangkan XML harus dilakukan sesuai urutan.

sumber:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html